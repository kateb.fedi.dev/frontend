import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import formus module
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
// routing module
import { AppRoutingModule } from './app-routing.module';

// httpclient
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import service
// import angular material
import { AppMaterialModule } from 'src/shared/app-material.module';
import { LoginComponent } from './components/login/login.component';
import { ListComponent } from './components/list/list.component';
import { Interceptor } from './Interceptor';
import { OfferDetailComponent } from './components/offer-detail/offer-detail.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    OfferDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppMaterialModule,
  ],
  providers: [ {provide: HTTP_INTERCEPTORS,
    useClass: Interceptor,
    multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
