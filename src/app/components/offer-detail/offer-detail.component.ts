import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/services/login.service';
import { OffersService } from 'src/services/offers.service';

@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.component.html',
  styleUrls: ['./offer-detail.component.scss']
})
export class OfferDetailComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  dataset: [] = [];
  displayedColumns: string[] = ['id' , 'name',  'type'];
 id: string = ""
  dataSource: any

  constructor(
    private offersService: OffersService,
    private loginService: LoginService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
   this.route.params.subscribe(params => {
      this.id = params['id']
    });


    this.offersService.getOfferDetail(this.id).subscribe((res) => {
      this.dataset = res.subscriptions
      this.dataSource = new MatTableDataSource<any>(this.dataset);
      this.dataSource.paginator = this.paginator;
    })

  }


  logout(){
    this.loginService.logout();
  }




}
