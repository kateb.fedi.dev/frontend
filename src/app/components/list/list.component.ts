import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from "@angular/material/table";
import { Router } from '@angular/router';
import { LoginService } from 'src/services/login.service';
import { OffersService } from 'src/services/offers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  dataset: [] = [];
  displayedColumns: string[] = ['id' , 'contractStartDate','contractEndDate', 'name',  'action'];

  dataSource: any

  constructor(
    private offersService: OffersService,
    private loginService: LoginService,
    private route:Router
  ) { }

  ngOnInit(): void {
    this.offersService.getOffers().subscribe((res) => {
      this.dataset = res.offers
      this.dataSource = new MatTableDataSource<any>(this.dataset);
      this.dataSource.paginator = this.paginator;
    })

  }


  logout(){
    this.loginService.logout();
  }

goToDetail(id :string){
  this.route.navigate([`/list/${id}`]);
}


}
