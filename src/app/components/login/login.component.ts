import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { LoginService } from 'src/services/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 public loginValid = true;
 public username = '';
 public password = '';
 public form: FormGroup | undefined ;

  private _destroySub$ = new Subject<void>();
  private readonly returnUrl: string;

  constructor(
    private loginService: LoginService,
    private _route: ActivatedRoute,
    private _router: Router,
    protected fb: FormBuilder,
  ) {
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/list';
  }

  public ngOnInit(): void {
  }


  buildForm(): FormGroup {
    return this.form = this.fb.group(
      {
        username: ['',Validators.required],
        password: ["",Validators.required],
      }
    );
  }

  public ngOnDestroy(): void {
    this._destroySub$.next();
  }

  public onSubmit(): void {
    this.loginValid = true;

    this.loginService.authentification(this.username, this.password).pipe(take(1)).subscribe({
      next: (res) => {
        this.loginValid = true;
        this._router.navigateByUrl('/list');
        localStorage.setItem('id_token', res.authToken);
      },
      error: _ => this.loginValid = false
    });
  }
}

