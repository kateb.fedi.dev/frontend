import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  uri: string = environment.authentication
  urilogout: string = environment.logout

  constructor(
    private http: HttpClient
  ) { }

  /**
   * 
   * @param username 
   * @param password 
   * @returns 
   */
  public authentification(username: string, password: string): Observable<any> {

 
    let object = {
    
        "USERNAME": username,
        "PASSWORD": password
    }

    return this.http.post<any>(`${this.uri}`, object/* , { headers: headers } */);

  }


  public logout(captureLastUrl: boolean = false) {
  
    this.http.get(this.urilogout)
      .subscribe(() => {
        localStorage.setItem('id_token', '');
      });
  }

}