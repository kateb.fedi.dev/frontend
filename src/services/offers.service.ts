import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  uri: string = environment.API
  headers = new HttpHeaders()
  constructor(
    private http: HttpClient
  ) { }

  /**
   * 
   * @param username 
   * @param password 
   * @returns 
   */
  public getOffers(): Observable<any> {



    return this.http.get<any>(`${this.uri}`, { headers: this.headers });

  }


  getOfferDetail(id : string){
    return this.http.get<any>(`${this.uri}/${id}/subscriptions`, { headers: this.headers });

  }



}